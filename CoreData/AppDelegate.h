//
//  AppDelegate.h
//  CoreData
//
//  Created by Harvey Nash on 9/7/17.
//  Copyright © 2017 Harvey Nash. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

